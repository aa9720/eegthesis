#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 17:49:27 2021

@author: allard
"""
import matplotlib.pyplot as p
import numpy as np
import pandas as pd
import os

p.style.use('default')

f, sbps = p.subplots(1,2, sharey=True)
f.set_size_inches(8,4.5)
fileLoc = os.path.dirname(__file__)

btcPath = fileLoc+'/sup/gemini_BTCUSD_2020_1min.csv'
ethPath = fileLoc+'/sup/gemini_ETHUSD_2020_1min.csv'

eth = pd.read_csv(ethPath, skiprows=1)
btc = pd.read_csv(btcPath, skiprows=1)


eth['Close'].hist(ax=sbps[0], bins=100, alpha=0.95)
btc['Close'].hist(ax=sbps[0], bins=100, alpha=0.95)
sbps[0].legend(['ETH', 'BTC'])
sbps[0].set_title('ETH and BTC histograms')

norm = lambda e: (e['Close'] - e['Close'].mean()) / e['Close'].std()

eth['ClNorm'] = norm(eth)
btc['ClNorm'] = norm(btc)


eth['ClNorm'].hist(ax=sbps[1], bins=100, alpha=0.75)
btc['ClNorm'].hist(ax=sbps[1], bins=100, alpha=0.75)
sbps[1].legend(['ETH', 'BTC'])
sbps[1].set_title('Normalized ETH and BTC histograms')
# f.legend(['ETH', 'BTC'])
f.set_tight_layout(True)

import tikzplotlib
tikzplotlib.clean_figure()
print(tikzplotlib.get_tikz_code())
