#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 11:36:12 2022

@author: allard
"""
# Borrowed from: https://gist.github.com/craffel/2d727968c3aaebd10359
# Heavily modifed though...


import matplotlib.pyplot as p
import numpy as np
import scipy.stats as stats


def draw_neural_net(ax, left, right, bottom, top, layer_sizes, layerNames=None):
    '''
    Draw a neural network cartoon using matplotilb.

    :usage:
        >>> fig = plt.figure(figsize=(12, 12))
        >>> draw_neural_net(fig.gca(), .1, .9, .1, .9, [4, 7, 2])

    :parameters:
        - ax : matplotlib.axes.AxesSubplot
            The axes on which to plot the cartoon (get e.g. by plt.gca())
        - left : float
            The center of the leftmost node(s) will be placed here
        - right : float
            The center of the rightmost node(s) will be placed here
        - bottom : float
            The center of the bottommost node(s) will be placed here
        - top : float
            The center of the topmost node(s) will be placed here
        - layer_sizes : list of int
            List of layer sizes, including input and output dimensionality
    '''
    n_layers = len(layer_sizes)
    v_spacing = (top - bottom)/float(max(layer_sizes))
    h_spacing = (right - left)/float(len(layer_sizes) - 1)
    circleR = v_spacing/8.
    if layerNames is None or len(layerNames) < n_layers:
        layerNames = [None] * n_layers
    # Nodes
    for n, layer_size in enumerate(layer_sizes):
        layer_top = v_spacing*(layer_size - 1)/2. + (top + bottom)/2.
        for m in range(layer_size):
            circle = p.Circle((n*h_spacing + left, layer_top - m*v_spacing),
                              circleR,
                              color='w', ec='k', zorder=4)
            ax.add_artist(circle)
        if layerNames[n] is not None:
            tx = p.Text(n*h_spacing + left,
                        layer_top + 0.05,
                        layerNames[n],
                        c='darkblue')
            ax.add_artist(tx)
    # Edges
    arrowColors = ['b', 'g', 'r', 'orange', 'y', 'k']
    for n, (layer_size_a, layer_size_b) in enumerate(zip(layer_sizes[:-1], layer_sizes[1:])):
        layer_top_a = v_spacing*(layer_size_a - 1)/2. + (top + bottom)/2.
        layer_top_b = v_spacing*(layer_size_b - 1)/2. + (top + bottom)/2.
        for m in range(layer_size_a):
            for o in range(layer_size_b):
                # line = p.Line2D([n*h_spacing + left, (n + 1)*h_spacing + left],
                #                   [layer_top_a - m*v_spacing, layer_top_b - o*v_spacing], c='k')
                x0, y0 = (n + 1)*h_spacing+left, layer_top_b - o*v_spacing
                x1, y1 = n*h_spacing + left, layer_top_a - m*v_spacing
                ang = np.angle(x1 - x0 + 1j*y1 - 1j*y0)
                xOffset = circleR * np.cos(ang)
                yOffset = circleR * np.sin(ang)
                line = p.Annotation('',
                                    (x0 + xOffset, y0 + yOffset),
                                     (x1 - xOffset, y1 - yOffset),
                                     arrowprops={'headlength': 6,
                                                 'headwidth': 4,
                                                 'shrink': 0.0,
                                                 'fc': arrowColors[m%len(arrowColors)],
                                                 'ec': arrowColors[m%len(arrowColors)],
                                                 'width': 0.001})
                ax.add_artist(line)

fig = p.figure(figsize=(6, 7))
ax = p.subplot2grid((5,4), (0,0), colspan=4, rowspan=4)
ax.axis('off')
layNames = ['Input', 'Hidden0', 'Hidden1', 'Output']
draw_neural_net(ax, 0.1, 0.9, 0, 1, [4, 4, 3, 1], layNames)


hists = [[(1, 0.7, 5), (1.1, 0.6, 3), (1.2, 0.4, 1.6)],
         [(0, 0.45, 1), (1.1, 0.13, 1.5), (1.5, 0.3, 1)],
         [(-1, 0.3, 2), (1.1, 0.3, 1.5), (-1.2, 1, 2)],
         [(3, 0.3, 2), (-2, 0.3, 1.5), (1, 1.3, 5)]]

for i in range(4):
    ax1 = p.subplot2grid((5,4), (4, 0 + i))
    for pa in range(3):
        x = np.linspace(-5, 5, 200)
        y = stats.norm.pdf(x, *hists[i][pa][:2]) * hists[i][pa][2]
        if i == 0:
            ax1.plot(x,y, label='Pass %g' % pa)
        else:
            ax1.plot(x,y)
    ax1.set_title('{0} weight\ndistribution'.format(layNames[i]))
    ax1.set_ylim(0, 5)
    ax1.set_xlim(-5, 5)
    ax1.grid()

fig.legend(loc='center right')
fig.set_tight_layout(True)

p.savefig('fig.pdf', backend='pgf')

# import tikzplotlib
# tikzplotlib.clean_figure()
# print(tikzplotlib.get_tikz_code())
