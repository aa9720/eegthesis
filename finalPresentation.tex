\documentclass[aspectratio=169]{beamer}

\usetheme{Madrid}

\usepackage{soul}
\usepackage{pythontex}
\usepackage{xcolor}
\usepackage[absolute,overlay]{textpos}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\title{Analysis of Preictal EEG classification using AGrad-CAM\\(Augmented Gradient Class Activation Mapping)}
\subtitle{Thesis Defense}
\author{Andrew Allard}
\institute{Masters Thesis}
\date{April 15, 2022}

\definecolor{lightblue}{rgb}{0.1,0.2,.6}
\definecolor{darkred}{rgb}{0.8,0,0}

\newcommand{\etal}{\textit{et al. }}

\setbeamercolor{titlelike}{parent=structure,fg=lightblue}
%\setbeamercolor{frametitle}{bg=darkgray}
\setbeamercolor{author}{fg=black}
\setbeamercolor{institute}{fg=black}
\setbeamercolor{date}{fg=black}


\setbeamercolor*{bibliography entry author}{fg=black}
\setbeamercolor{bibliography entry note}{fg=black}
\captionsetup[figure]{labelfont={color=lightblue},textfont={color=black}}

\graphicspath{ {./figures/} }

\def \anim {1}

\usebackgroundtemplate%
{%
    \includegraphics[width=\paperwidth,
    height=\paperheight,
%    decodearray={0.1 0.2 0.1 0.2 0.1 0.2}]{brainBG.jpg}%
]{brainBG_bw.jpg}%
}


%\begin{textblock*}{5cm}(3cm,1cm) % {block width} (coords)
%\includegraphics[width=5cm]{images/logo.jpg}
%\end{textblock*}

\begin{document}


\begin{frame}
\titlepage
\end{frame}

\begin{frame}
	\hspace*{-0.4cm}\includegraphics[width=\paperwidth]{sup/topIceMar5.jpg}
\end{frame}


\section{Intro}
\begin{frame}{Overview}
\begin{itemize}%[<+->]
	\item EEG (Electroencephalography) and CNN (Convolutional Neural Networks) overview.
	\item Seizure prediction using CNNs.
	\item EEG epochs vs image.
	\item Neural Network architecture used.
	\item Augmented Grad-CAM.
	\item Data Preprocessing and Training.
	\item AGrad-CAM results.
	\item Discussion.
\end{itemize}
\end{frame}

\section{EEG}
\begin{frame}[allowframebreaks]{EEG}
\begin{columns}
	\column{\dimexpr0.4\linewidth}
	Electrode example
	\begin{figure}[h]
		EEG Electrode spacing\cite{BibEntry2022Feb}
		\begin{subfigure}{\columnwidth}
            \centering
			\includegraphics[height=0.15\textheight, trim=0 370 0 0, clip]{sensorPos1020}
		\end{subfigure}
	Montage \cite{BibEntry2017Jun}
	\begin{subfigure}{\columnwidth}
		\centering
		\includegraphics[height=0.25\textheight]{montage1020}
	\end{subfigure}
		Montage from CHB-MIT
		\vfill
		\begin{subfigure}{\columnwidth}
            \centering
			\includegraphics[height=0.25\textheight]{chb-mit_montage}
		\end{subfigure}
	\end{figure}
	\column{\dimexpr0.6\linewidth}
	Example of a 5 second EEG epoch.
	\begin{figure}[h]
		\includegraphics[width=\columnwidth]{eeg-epoch}
	\end{figure}
\end{columns}
\framebreak
\begin{figure}[h]
	\centering
	\includegraphics[height=0.88\textheight]{eegExample}
\end{figure}
\end{frame}

\section{Convolution}
\begin{frame}[allowframebreaks]{Convolution}
	\begin{columns}
		\column{\dimexpr0.5\linewidth}
		Convolution example
		\begin{figure}[h]
			\begin{subfigure}{\columnwidth}
				\includegraphics[width=\linewidth, trim=0 0 0 0, clip]{convParts}
			\end{subfigure}
		\vfill
		\begin{subfigure}{\columnwidth}
			\includegraphics[width=\linewidth]{convProcess}
		\end{subfigure}
		\end{figure}
		\column{\dimexpr0.5\linewidth}
		\begin{itemize}
			\item The feature is a collection of numbers trained during training.
			\item The output of convolution maintains spatial information.
		\end{itemize}
	\end{columns}
\framebreak
	\begin{figure}[h!]
	\centering
	\includegraphics[height=0.5\textheight]{figures/nnFormatFinal}
	\caption{Architecture of the neural network}
	\label{fig:nnft1}
	
\end{figure}
\tiny{Each of the convolution layers has 32 features of size (2,3).  Each of the strided convolution layers are of size (2,2).  The bidirectional LSTM has 20 LSTM cells in both directions.  The fully connected output layer is a single cell. This is a modified version of \cite{daoud_efficient_2019}. All convolution Springenberg \etal 2014 \cite{Springenberg_striving_2014Dec}}
\end{frame}

\begin{frame}{CNNs for Seizure prediction}
	\begin{itemize}
		\setlength\itemsep{0.5cm}
		\item Daoud \etal, 2019 Prediction of seizures using raw EEG data \cite{daoud_efficient_2019}.
		\item Pria Prathaban \etal, 2021 Prediction of seizures using sparse image construction \cite{priya_prathaban_dynamic_2021}.
		\item Rasheed \etal, 2021 DC-GANs to generate synthetic EEG data \cite{Rasheed_a_2021Nov}.
        \item Takahashi \etal, 2020 Used CNNs to monitor EEG images \cite{takahashi_convolutional_2020}.
	\end{itemize}
\end{frame}

\begin{frame}{Approach}
	\begin{itemize}%[<+->]
		\setlength\itemsep{0.5cm}
		\item Train patient specific neural networks similar to Daoud \etal \cite{daoud_efficient_2019}.
		\item Apply AGrad-CAM to the convolution portion of the NN.
		\item Overlay high resolution heatmaps on EEG to find features.
	\end{itemize}
\end{frame}

\section{Grad-CAM}
\begin{frame}[allowframebreaks]{Grad-CAM}
	\begin{itemize}
        \item Proposed by Selvaraju \etal, 2017 in \cite{Selvaraju_grad_2017Oct}.
        \item Performed on the final convolution layer.
        \item Generates a heatmap on a reduced version of an image.
        \item A down-sampled image still contains enough spatial information to see what it contains.
        \item EEG epochs do not have enough information at the last convolution layer for this to be useful.
        \begin{itemize}
            \item The EEG epoch at the last convolution layer has been reduced to a single array.
            \item CHB-MIT the last convolution layer output shape is (1x156).
            \item UR the last convolution layer output shape is (1x316).
        \end{itemize}
        \item There is no way to trace the Grad-CAMs backward through the network.
	\end{itemize}
    \begin{figure}[h]
        \centering
        \includegraphics[height=0.8\textheight]{gradCamThroughNet_2.png}
    \end{figure}
\end{frame}
\section{Augmentation}
\begin{frame}[allowframebreaks]{Augmentation Example}
	\begin{columns}
		\column{\dimexpr0.5\linewidth}
		\begin{figure}[h]
			\centering
			\includegraphics[width=\textwidth]{sup/blueIceInked}
			\caption{Image of Ice flow}
			\label{fig:blueice}
		\end{figure}
		\column{\dimexpr0.5\linewidth}
		\begin{figure}[h]
			\centering
			\includegraphics[width=\textwidth]{sup/blueIceMultiAug}
			\caption{Augmented version of the Ice image}
			\label{fig:blueiceAug}
		\end{figure}
	\end{columns}
\framebreak
	\begin{columns}
	\column{\dimexpr0.5\linewidth}
	\begin{figure}[h]
		\centering
		\includegraphics[width=\textwidth]{eegEP_nonAug}
		\caption{An EEG epoch}
		\label{fig:eegep}
	\end{figure}
	\column{\dimexpr0.5\linewidth}
	\begin{figure}[h]
		\centering
		\includegraphics[width=\textwidth]{eegEP_augMulti}
		\caption{Augmented version of the EEG epoch}
		\label{fig:eegepAug}
	\end{figure}
\end{columns}
\end{frame}
\begin{frame}[allowframebreaks]{EEG Epoch VS Image}
	\begin{columns}
		\column{\dimexpr0.5\linewidth}
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=\columnwidth]{eegepochVsImage.png}
			\caption{Aspect ratios of EEG epoch vs Image (not to scale)}
			\label{fig:imageVsEEGep}
		\end{figure}
		\column{\dimexpr0.5\linewidth}
		\begin{itemize}
			\item Aspect ratios (1280:23 EEG epoch 16:9 Image).
			\item Images typically concentrate content toward the center.
			\item EEG epochs can have content anywhere.
		\end{itemize}
	\end{columns}
\end{frame}
\section{AGrad-CAM}
\begin{frame}[allowframebreaks]{Augmented Grad-CAM}
	\begin{columns}
		\column{\dimexpr0.6\linewidth}
		\begin{itemize}
		\item Proposed by Morbidelli \etal 2020 in \cite{Morbidelli_augmented_2020May}.
   		\item If an image is augmented and Grad-CAM performed the resulting Grad-CAM is part of a higher resolution heatmap dataset.
        \item Setup an optimization problem to solve for a high resolution heatmap using all the lower resolution Grad-CAMs
        \begin{itemize}
            \item Augment an image many times (99 used in \cite{Morbidelli_augmented_2020May} 100 in this work).
            \item Perform Grad-CAM on each of these augmented images.
            \item Set up an optimization problem.
        \end{itemize}
	\end{itemize}
\column{\dimexpr0.4\linewidth}
\begin{figure}[h]
	\centering
	\includegraphics[width=\columnwidth]{agradcams/CHBpat1Preictal_1.png}
	\caption{CHB-MIT patient 1 Preictal EEG epoch with AGrad-CAM heatmap}
\end{figure}
	\end{columns}
\framebreak
From Morbidelli \etal 2020 \cite{Morbidelli_augmented_2020May}.
\begin{equation*}
    \mathop {\min }\limits_{\mathbf{h}} \frac{1}{2}\sum\limits_{l = 1}^L {\left\| {\mathcal{D}{A_l}h - {{\mathbf{g}}_l}} \right\|_2^2} + \lambda {\operatorname{TV} _{{\ell _1}}}({\mathbf{h}}) + \frac{\mu }{2}\left\| {\mathbf{h}} \right\|_2^2
\end{equation*}
\begin{equation*}
    {\text{T}}{{\text{V}}_{{\ell _1}}}({\mathbf{h}}) = \sum\limits_{i,j} {\left| {{\partial _x}{\mathbf{h}}(i,j)} \right|} + \left| {{\partial _y}{\mathbf{h}}(i,j)} \right|
\end{equation*}
Where:
\begin{itemize}
    \item $\mu$ and $\lambda$ is a scaling constant.
    \item ${{\mathbf{g}}_l}$ is the low resolution grad-CAM.
    \item $\mathcal{D}{A_l}h$ is the high resolution heatmap augmented.
    \item This was implemented in the python library Tensorflow \cite{tensorflow2015_whitepaper}.
\end{itemize}
\end{frame}


\section{Thesis}
\begin{frame}[allowframebreaks]{Neural Network Architecture}
	\begin{figure}[h!]
		\centering
		\includegraphics[height=0.5\textheight]{figures/nnFormatFinal}
		\caption{Architecture of the neural network}
		\label{fig:nnft}

	\end{figure}
		\tiny{Each of the convolution layers has 32 features of size (2,3).  Each of the strided convolution layers are of size (2,2).  The bidirectional LSTM has 20 LSTM cells in both directions.  The fully connected output layer is a single cell. This is a modified version of \cite{daoud_efficient_2019}.}
\end{frame}


\begin{frame}[allowframebreaks]
	\frametitle{Data used}
	\begin{columns}
		\small
		\column{\dimexpr0.5\textwidth}
		\begin{itemize}
		\item CHB-MIT dataset \cite{Shoeb_application_2009} \cite{PhysioNet}.
		\begin{itemize}
			\item Published mid 2000 \cite{PhysioNet}.
			\item Used in many publications.
			\item Patients were all children.
			\item \textit{Patients 1, 3, 7, 21 were used for this study}.

		\end{itemize}
        \item Data provided by UR.
        \begin{itemize}
        	\item Anonymized data.
        	\item Adult patients.
        	\item Data from the last few years.
        	\item \textit{Patients 1 and 6 were used in this study}.
        \end{itemize}
	\end{itemize}
\column{\dimexpr0.5\textwidth}
\begin{itemize}
	\item CHB-MIT data was formatted in many small files with short breaks between.
	\item UR data was given in a few large files with very small breaks between files.
\end{itemize}
	\end{columns}
\framebreak
\begin{table}[h]
	\centering
	\begin{tabular}{|| c | c | c | c | c ||}
		\hline
		CHB Num & Train S File \# & \# Train Files & Aug S File & \# Aug Files \\
		\hline
		\hline
		1 & 0 & 22 & 0 & 6 \\
		\hline
		3 & 0 & 12 & 30 & 6 \\
		\hline
		7 & 0 & \textit{all} & 10 & 6 \\
		\hline
		21 & 0 & \textit{all} & 16 & 6 \\
		\hline
	\end{tabular}
\end{table}
\begin{table}[h]
	\centering
	\begin{tabular}{|| c | c | c | c | c ||}
		\hline
		UR Num & Train S File \# & \# Train Files & Aug S File & \# Aug Files \\
		\hline
		\hline
		1 & 0 & \textit{all} & 2 & 1 \\
		\hline
		6 & 0 & \textit{all} & 2 & 1 \\
		\hline
	\end{tabular}
\end{table}
Where:
\begin{itemize}
	\item 'Train S File \#' is the index of the first file used for training.
	\item '\# Train Files' is the number of files used to train the NN from the initial index.
	\item 'Aug S File' is the index of the first file used for augmentation.
	\item '\# Aug Files' the number of files used from the initial index.
\end{itemize}
\end{frame}

\section{Results}
\begin{frame}{Training Results CHB-MIT}
	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{figures/pat1}
		\caption{NN predictions and ROC for patient 1}
		\label{fig:pat1fit}
	\end{figure}
\end{frame}

\begin{frame}{Training Results UR}
	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{figures/URpat1}
		\caption{NN predictions and ROC for patient 1 from the UR}
		\label{fig:URpat1fit}
	\end{figure}
\end{frame}


\begin{frame}{Example AGrad-CAMs}
	
	\begin{columns}
		\column{\dimexpr0.5\textwidth}
		\begin{figure}[h]
			\centering
			\includegraphics[width=\columnwidth]{agradcams/CHBpat1Preictal_1FixedRatio.png}
			\caption{CHB-MIT patient 1 Preictal EEG epoch with AGrad-CAM heatmap}
		\end{figure}
		\column{\dimexpr0.5\textwidth}
		\begin{figure}[h]
			\centering
			\includegraphics[width=\columnwidth]{agradcams/URpatient1EEGPreictal_1FixedRatio.png}
			\caption{UR patient 1 Preictal EEG epoch with AGrad-CAM heatmap}
		\end{figure}
	\end{columns}
\end{frame}


\begin{frame}{Probability of a the max occurring in time}
	
	\begin{columns}
		\column{\dimexpr0.5\textwidth}
		\begin{figure}[h]
			\centering
			\includegraphics[width=\columnwidth]{pat1_augTimeProb}
			\caption{CHB-MIT patient 1 Probability of a slice of time being where the maximum occurs}
		\end{figure}
		\column{\dimexpr0.5\textwidth}
		\begin{figure}[h]
			\centering
			\includegraphics[width=\columnwidth]{URpat1_augTimeProb}
			\caption{UR patient 1 Probability of a slice of time being where the maximum occurs}
		\end{figure}
	\end{columns}
\end{frame}


\begin{frame}{Probability of channels being used for classification}

\begin{columns}
	\column{\dimexpr0.5\textwidth}
	\begin{figure}[h]
		\centering
		\includegraphics[width=\columnwidth]{pat1_augChannelProb}
		\caption{CHB-MIT patient 1 Probability of a channel being the most impactful for classification}
	\end{figure}
\column{\dimexpr0.5\textwidth}
\begin{figure}[h]
	\centering
	\includegraphics[width=\columnwidth]{URpat1_augChannelProb}
	\caption{UR patient 1 Probability of a channel being the most impactful for classification}
\end{figure}
\end{columns}
\end{frame}


\section{End}
\begin{frame}{Summary}
	\begin{itemize}
		\setlength\itemsep{0.5cm}
		\item No obvious correlation between classification neural network and EEG activity.
		\item Results indicate certain channels have greater impact on the neural network.
		\item Results indicate a different augmentation process needs to be developed.
%		\begin{itemize}
%			\item Further research needs to be done to find a better approach.
%		\end{itemize}
	\end{itemize}
\end{frame}


\begin{frame}{Questions}
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.6\textwidth]{netQuestion}
		\caption{Are there any questions?}
	\end{figure}
\end{frame}

\begin{frame}{Thank you to...}
	\begin{itemize}
		\item Dr Phillips for taking me on as an graduate student.
		\item Dr Berg, Steve Erickson.
		\begin{itemize}
			\item The UR for providing a second EEG data source.
			\item EEG and seizure expertise and advice.
		\end{itemize}
		\item Dr Markopoulos, Dr Rabbani, and Dr Berg for being on my committee.
		\item Some python packages used heavily:
        \begin{itemize}
            \item Tensorflow \cite{tensorflow2015_whitepaper}.
            \item Scikit-Learn \cite{scikit_learn}.
            \item MNE-Python \cite{GramfortEtAl2013a}
            \item plotly \cite{plotly}.
        \end{itemize}
	\end{itemize}
\end{frame}

\section{Supplemental}

\begin{frame}{Potential paths forward}
	\begin{itemize}
		\setlength\itemsep{0.4cm}
		\item Design a better fitting Neural Network.
		\item Investigate alternative augmentation procedures for AGrad-CAM.
		\item Perform Frequency Analysis of the EEG highlighted by AGrad-CAM.
		\item Evaluate the use Transposed CNN as proposed by Zeiler \etal 2013 \cite{Zeiler_visualizing_2013Nov}.
	\end{itemize}
\end{frame}

\begin{frame}{Training Results CHB-MIT}
	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{figures/pat3}
		\caption{NN predictions and ROC for patient 3}
		\label{fig:pat3fit}
	\end{figure}
\end{frame}

\begin{frame}{Training Results CHB-MIT}
	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{figures/pat7}
		\caption{NN predictions and ROC for patient 7}
		\label{fig:pat7fit}
	\end{figure}
\end{frame}

\begin{frame}{Training Results CHB-MIT}
	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{figures/pat21}
		\caption{NN predictions and ROC for patient 21}
		\label{fig:pat21fit}
	\end{figure}
\end{frame}

\begin{frame}{Training Results UR}
	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{figures/URpat6}
		\caption{NN predictions and ROC for patient 6 from the UR}
		\label{fig:URpat6fit}
	\end{figure}
\end{frame}

\section{Bibliography}
\begin{frame}[allowframebreaks]
\tiny
\bibliographystyle{ieeetr}
\bibliography{seizurePrediction.bib}
\end{frame}

\end{document}

